const name = document.getElementById("name");
const roll_num = document.getElementById("roll_no");
const form = document.getElementById("form");
const errorElement = document.getElementById("error");

form.addEventListener("submit", (e) => {
  let messages = [];
  if (name.value === "" || name.value == null) {
    messages.push("Name is required");
  }

  if (password.value.length <= 9) {
    messages.push("Please enter your roll number");
  }

  if (roll_num.value.length >= 20) {
    messages.push("Enter your roll number starting from CCOEW");
  }

  if (password.value === "name") {
    messages.push("Roll Number cannot be same as your name");
  }

  if (messages.length > 0) {
    e.preventDefault();
    errorElement.innerText = messages.join(", ");
  }
});

//object to store data to localStorage

let myObj = {
  name: "Anushka Kalamkar",
  roll_number: "CCOEW4520",
};
localStorage.setItem("myObj", myObj);
console.log(localStorage);
